# flup

[**flup**](https://pypi.org/project/flup)
![PyPI - Downloads](https://img.shields.io/pypi/dm/flup)
Random assortment of WSGI servers

![Popularity contest statistics for python-flup](https://qa.debian.org/cgi-bin/popcon-png?packages=python-flup&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y-%25m)

[![Packaging status](https://repology.org/badge/vertical-allrepos/python:flup.svg)](https://repology.org/project/python:flup/versions)

* Python 2 packages, see also flup6.

## Unofficial documentation
* [*Apache, FastCGI and Python*
  ](https://www.electricmonk.nl/docs/apache_fastcgi_python/apache_fastcgi_python.html)
  2017-10 Ferry Boender
